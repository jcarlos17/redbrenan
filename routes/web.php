<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth', 'admin'], 'namespace' => 'Admin'], function () {
    Route::get('format', 'TicketController@index');
    Route::group(['prefix' => 'students'], function () {
        Route::get('', 'StudentController@index');
        Route::get('create', 'StudentController@create');
        Route::post('create', 'StudentController@store');
        Route::get('{id}/edit', 'StudentController@edit');
        Route::post('{id}/edit', 'StudentController@update');
        Route::get('{id}/delete', 'StudentController@delete');
        Route::get('download', 'StudentController@download');
    });
});

Route::group(['middleware' => ['auth'], 'namespace' => 'User'], function () {
    Route::get('payments', 'PaymentController@index');
});
