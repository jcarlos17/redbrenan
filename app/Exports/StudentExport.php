<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class StudentExport implements FromView
{
    protected $students;

    public function __construct($students = null)
    {
        $this->students = $students;
    }

    public function view(): View
    {
        return view('exports.students', [
            'students' => $this->students
        ]);
    }
}