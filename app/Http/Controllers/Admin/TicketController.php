<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

class TicketController extends Controller
{
    public function index()
    {
        $pdf = PDF::loadView('admin.tickets.index-pdf')
            ->setPaper('a4', 'landscape');

        return $pdf->stream('boleta.pdf');
    }
}
