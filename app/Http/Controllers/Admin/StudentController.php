<?php

namespace App\Http\Controllers\Admin;

use App\Exports\StudentExport;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class StudentController extends Controller
{
    public function index()
    {
        $students = User::where('role', User::USER)->paginate(15);

        return view('admin.students.index', compact('students'));
    }

    public function create()
{
    return view('admin.students.create');
}

    public function store(Request $request)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'address' => ['required', 'string', 'max:255'],
            'code' => 'required',
            'dni' => 'required',
            'birthday' => 'required',
            'phone' => 'required',
        ];

        $messages = [
            'name.required' => 'Es necesario este campo',
            'name.max' => 'Se acepta un máximo de 255 caracteres',
            'email.required' => 'Es necesario este campo',
            'email.email' => 'No tiene un formato email',
            'email.max' => 'Se acepta un máximo de 255 caracteres',
            'email.unique' => 'Ya se encuentra registrado',
            'password.required' => 'Es necesario este campo',
            'password.min' => 'Se acepta un mínimo de 8 caracteres',
            'address.required' => 'Es necesario este campo',
            'address.max' => 'Se acepta un máximo de 255 caracteres',
            'code.required' => 'Es necesario este campo',
            'dni.required' => 'Es necesario este campo',
            'birthday.required' => 'Es necesario este campo',
            'phone.required' => 'Es necesario este campo',
        ];

        $this->validate($request, $rules, $messages);

        $birthday = Carbon::createFromFormat('d/m/Y', $request->birthday);

        $student = New User();
        $student->name = $request->name;
        $student->email = $request->email;
        $student->password = bcrypt($request->password);
        $student->address = $request->address;
        $student->code = $request->code;
        $student->dni = $request->dni;
        $student->birthday = $birthday;
        $student->phone = $request->phone;
        //notes
        $student->note_1 = $request->note_1;
        $student->note_2 = $request->note_2;
        $student->save();

        return redirect('students')->with('notification', 'Se registró correctamente');

    }

    public function edit($id)
    {
        $student = User::findOrFail($id);

        return view('admin.students.edit', compact('student'));
    }

    public function update($id, Request $request)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],
            'password' => ['nullable', 'string', 'min:8'],
            'address' => ['required', 'string', 'max:255'],
            'code' => 'required',
            'dni' => 'required',
            'birthday' => 'required',
            'phone' => 'required',
        ];

        $messages = [
            'name.required' => 'Es necesario este campo',
            'name.max' => 'Se acepta un máximo de 255 caracteres',
            'email.required' => 'Es necesario este campo',
            'email.email' => 'No tiene un formato email',
            'email.max' => 'Se acepta un máximo de 255 caracteres',
            'email.unique' => 'Ya se encuentra registrado',
            'password.min' => 'Se acepta un mínimo de 8 caracteres',
            'address.required' => 'Es necesario este campo',
            'address.max' => 'Se acepta un máximo de 255 caracteres',
            'code.required' => 'Es necesario este campo',
            'dni.required' => 'Es necesario este campo',
            'birthday.required' => 'Es necesario este campo',
            'phone.required' => 'Es necesario este campo',
        ];

        $this->validate($request, $rules, $messages);

        $birthday = Carbon::createFromFormat('d/m/Y', $request->birthday);

        $student = User::findOrFail($id);
        $student->name = $request->name;
        $student->email = $request->email;
        $password = $request->password;
        if ($password)
            $student->password = bcrypt($password);

        $student->address = $request->address;
        $student->code = $request->code;
        $student->dni = $request->dni;
        $student->birthday = $birthday;
        $student->phone = $request->phone;
        //notes
        $student->note_1 = $request->note_1;
        $student->note_2 = $request->note_2;
        $student->save();

        return redirect('students')->with('notification', 'Se guardaron los cambios correctamente');
    }

    public function delete($id)
    {
        User::findOrFail($id)->delete();

        return back()->with('notification', 'Se eliminó correctamente');
    }

    public function download()
    {
        $students = User::where('role', User::USER)->paginate(15);
        $export = new StudentExport($students);
        $fileName = "reporte_alumnos.xlsx";

        return Excel::download($export, $fileName);

    }
}
