<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!-- User -->
        <div class="user-box">
            <div class="user-img">
                <img src="{{ asset('/images/users/avatar-1.jpg') }}" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                <div class="user-status online"><i class="zmdi zmdi-dot-circle"></i></div>
            </div>
            <h5><a href="#">{{ auth()->user()->name }}</a> </h5>
            <ul class="list-inline">
                {{--<li>--}}
                    {{--<a href="#" >--}}
                        {{--<i class="zmdi zmdi-settings"></i>--}}
                    {{--</a>--}}
                {{--</li>--}}

                <li>
                    <a href="{{ url('/logout') }}"
                       onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" title="Cerrar sesión">
                        <i class="zmdi zmdi-power"></i>
                    </a>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
        <!-- End User -->
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
                <li class="text-muted menu-title">Menú</li>

                <li>
                    <a href="{{ url('/home') }}" class="waves-effect">
                        <i class="zmdi zmdi-home"></i>
                        <span> Inicio </span>
                    </a>
                </li>
                @if (auth()->user()->is(0))
                    <li>
                        <a href="{{ url('students') }}" class="waves-effect">
                            <i class="fa fa-users"></i> <span> Gestión de alumnos </span>
                        </a>
                    </li>
                @endif
                @if (auth()->user()->is(1))
                    <li>
                        <a href="{{ url('payments') }}" class="waves-effect">
                            <i class="fa fa-credit-card-alt"></i> <span> Pagos </span>
                        </a>
                    </li>
                @endif
            </ul>
            <div class="clearfix"></div>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>

</div>
<!-- Left Sidebar End -->