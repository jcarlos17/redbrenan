@extends('layouts.app')

@section('meta_title', 'Lista de alumnos | ' . config('app.name'))

@section('section_title', 'Alumnos')

@section('styles')

@endsection

@section('content')
    @include('includes.alert')
    <div class="row">
        <div class="col-sm-8">
            <a href="{{ url('students/create') }}" class="btn btn-success btn-rounded btn-bordred waves-effect waves-light w-md m-b-15 m-r-10">
                <i class="fa fa-user m-r-5"></i>Nuevo alumno
            </a>
        </div><!-- end col -->
    </div>
    <div class="card-box table-responsive">
        <div class="pull-right m-b-10">
            <a href="{{ url('/students/download') }}" class="btn btn-inverse btn-rounded btn-bordred waves-effect waves-light w-md m-b-15 m-r-10">
                <i class="fa fa-file-excel-o m-r-5"></i>Exportar Excel
            </a>
        </div>
        <h4 class="header-title m-t-0 m-b-30">Lista de alumnos</h4>
        <table class="table table-bordered table-hover">
            <thead>
            <tr class="active text-center">
                <th>Nombre</th>
                <th>Dirección</th>
                <th>Mes pendiente</th>
                <th class="col-sm-2 text-center">Último pago</th>
                <th class="text-center">Teléfono</th>
                <th class="text-center">DNI</th>
                <th class="col-sm-2 text-center">Acción</th>
            </tr>
            </thead>
            <tbody>
            @foreach($students as $student)
                <tr class="text-center">
                    <td>{{ $student->name }}</td>
                    <td>{{ $student->address }}</td>
                    <td>01-ago</td>

                    <td>01/08 pago por Julio</td>
                    <td>{{ $student->phone }}</td>
                    <td>{{ $student->dni }}</td>
                    <td>
                        <a href="{{ url('/students/'.$student->id.'/edit') }}" class="btn btn-sm btn-primary" title="Editar">
                            <i class="fa fa-pencil-square-o"></i>
                        </a>
                        <button class="btn btn-sm btn-danger" title="Eliminar" data-delete="{{ url('/students/'.$student->id.'/delete') }}">
                            <i class="fa fa-trash o"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $students->appends(Request::except('page'))->links() }}
    </div>
@endsection

@section('script')
    <!-- Sweet alert 2 -->
    <script src="https://unpkg.com/sweetalert2@7.3.0/dist/sweetalert2.all.js"></script>
    <script>
        $(document).ready(function() {
            $('[data-delete]').on('click', onClickDoctorDelete);
        });

        function onClickDoctorDelete() {
            let urlDelete = $(this).data('delete');
            swal({
                title: '¿Seguro que desea eliminar este alumno?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.value) {
                location.href = urlDelete;
            }
        });
        }
    </script>
@endsection