@extends('layouts.app')

@section('meta_title', 'Nuevo alumno | ' . config('app.name'))

@section('section_title', 'Nuevo alumno')

@section('styles')
    <link href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    @include('includes.alert')
    <form role="form"  action="" method="POST">
        @csrf
        <div class="row">
            <div class="col-sm-6">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-15 header-title"><b>Datos básicos</b></h4>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group @error('name') has-error @enderror">
                                <label for="name">Nombre</label>
                                <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}">
                                @error('name')
                                <span class="help-block"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group @error('address') has-error @enderror">
                                <label for="address">Dirección</label>
                                <input type="text" name="address" class="form-control" id="address" value="{{ old('address') }}">
                                @error('address')
                                <span class="help-block"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group @error('code') has-error @enderror">
                                <label for="code">Código cliente</label>
                                <input type="text" name="code" class="form-control" id="code" value="{{ old('code') }}">
                                @error('code')
                                <span class="help-block"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group @error('dni') has-error @enderror">
                                <label for="dni">DNI</label>
                                <input type="text" name="dni" class="form-control" id="dni" value="{{ old('dni') }}"
                                       placeholder="99999999" data-mask="99999999">
                                @error('dni')
                                <span class="help-block"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group @error('birthday') has-error @enderror">
                                <label for="birthday">Fecha de Nacimiento</label>
                                <input type="text" class="form-control" placeholder="dd/mm/yyyy" id="datepicker-autoclose" name="birthday" value="{{ old('birthday')}}">
                                @error('birthday')
                                <span class="help-block"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group @error('phone') has-error @enderror">
                                <label for="phone">Teléfono</label>
                                <input type="text" name="phone" class="form-control" id="phone" value="{{ old('phone') }}"
                                       placeholder="999 999 999" data-mask="999 999 999">
                                @error('phone')
                                <span class="help-block"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <h4 class="m-t-5 m-b-15 header-title"><b>Datos de la cuenta</b></h4>
                    <div class="form-group @error('email') has-error @enderror">
                        <label for="email">Correo</label>
                        <input type="email" name="email" class="form-control" id="email" value="{{ old('email') }}">
                        @error('email')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group @error('password') has-error @enderror">
                        <label for="password">Contraseña</label>
                        <input type="text" name="password" class="form-control" id="password">
                        @error('password')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-15 header-title"><b>Comentarios</b></h4>
                    <div class="form-group">
                        <label for="note_1">Nota 1</label>
                        <textarea class="form-control" name="note_1" id="note_1" rows="5">{{ old('note_1') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="note_2">Nota 2</label>
                        <textarea class="form-control" name="note_2" id="note_2" rows="5">{{ old('note_2') }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button class="btn btn-primary btn-rounded btn-bordred waves-effect waves-light w-md">
                Registrar
            </button>
            <a href="{{ url('students') }}" class="btn btn-danger btn-rounded btn-bordred waves-effect waves-light w-md">Cancelar</a>
        </div>
    </form>

@endsection

@section('script')
    <script src="{{ asset('plugins/bootstrap-inputmask/bootstrap-inputmask.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
    </script>
@endsection