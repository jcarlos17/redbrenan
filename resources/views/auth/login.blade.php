@extends('layouts.auth')

@section('meta_title', 'Iniciar sesión | ' . config('app.name'))

@section('content')
    <div class="m-t-40 card-box">
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">Iniciar sesión</h4>
        </div>

        <div class="panel-body">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}" autocomplete="off">
                @csrf

                <div class="form-group @error('email') has-error @enderror">
                    <div class="col-xs-12">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Correo" required autocomplete="off">
                        @error('email')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>

                <div class="form-group @error('password') has-error @enderror">
                    <div class="col-xs-12">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" required autocomplete="off">
                        @error('login')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>

                <div class="form-group text-center m-t-30">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-bordred btn-block waves-effect waves-light" type="submit">Ingresar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
