<table>
    <thead>
    <tr>
        <th style="border: 1px solid black; text-align: center">Nombre</th>
        <th style="border: 1px solid black; text-align: center">Dirección</th>
        <th style="border: 1px solid black; text-align: center">Mes Pendiente</th>
        <th style="border: 1px solid black; text-align: center">Último Pago</th>
        <th style="border: 1px solid black; text-align: center">Teléfono</th>
        <th style="border: 1px solid black; text-align: center">DNI</th>
        <th style="border: 1px solid black; text-align: center">Fecha Nacimiento</th>
        <th style="border: 1px solid black; text-align: center">Código Cliente</th>
        <th style="border: 1px solid black; text-align: center">Nota 01</th>
        <th style="border: 1px solid black; text-align: center">Nota 02</th>
    </tr>
    </thead>
    <tbody>
    @foreach($students as $student)
        <tr>
            <td style="border: 1px solid black; text-align: center">{{ $student->name }}</td>
            <td style="border: 1px solid black; text-align: center">{{ $student->address }}</td>
            <td style="border: 1px solid black; text-align: center">01 - ago</td>
            <td style="border: 1px solid black; text-align: center">01/08 pago por Julio</td>
            <td style="border: 1px solid black; text-align: center">{{ $student->phone }}</td>
            <td style="border: 1px solid black; text-align: center">{{ $student->dni }}</td>
            <td style="border: 1px solid black; text-align: center">{{ $student->birthday_format }}</td>
            <td style="border: 1px solid black; text-align: center">{{ $student->note_1 }}</td>
            <td style="border: 1px solid black; text-align: center">{{ $student->note_2 }}</td>
        </tr>
    @endforeach
    </tbody>
</table>