@extends('layouts.app')

@section('meta_title', 'Lista de pagos | ' . config('app.name'))

@section('section_title', 'Pagos')

@section('styles')

@endsection

@section('content')
    @include('includes.alert')
    <div class="card-box table-responsive">
        <h4 class="header-title m-t-0 m-b-30">Lista de pagos</h4>
        <table class="table table-bordered table-hover">
            <thead>
            <tr class="active">
                <th class="text-center">ID</th>
                <th class="text-center">Monto</th>
                <th class="text-center">Fecha pago</th>
                <th class="text-center">Fecha vencimiento</th>
                <th class="col-sm-2 text-center">Estado</th>
            </tr>
            </thead>
            <tbody>
            <tr class="text-center">
                <td>01</td>
                <td>123.00</td>
                <td>26/04/2016</td>
                <td>26/04/2016</td>
                <td><span class="label label-success">Realizado</span></td>
            </tr>
            <tr class="text-center">
                <td>01</td>
                <td>123.00</td>
                <td>26/04/2016</td>
                <td>26/04/2016</td>
                <td><span class="label label-success">Realizado</span></td>
            </tr>
            <tr class="text-center">
                <td>01</td>
                <td>123.00</td>
                <td>--</td>
                <td>26/04/2016</td>
                <td><span class="label label-danger">Pendiente</span></td>
            </tr>
            <tr class="text-center">
                <td>01</td>
                <td>123.00</td>
                <td>--</td>
                <td>26/04/2016</td>
                <td><span class="label label-danger">Pendiente</span></td>
            </tr>
            <tr class="text-center">
                <td>01</td>
                <td>123.00</td>
                <td>--</td>
                <td>26/04/2016</td>
                <td><span class="label label-danger">Pendiente</span></td>
            </tr>
            <tr class="text-center">
                <td>01</td>
                <td>123.00</td>
                <td>--</td>
                <td>26/04/2016</td>
                <td><span class="label label-danger">Pendiente</span></td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection

@section('script')

@endsection