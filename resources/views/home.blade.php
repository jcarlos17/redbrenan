@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if(auth()->user()->is(0))
                        <a href="{{ url('/format') }}" target="_blank" class="btn btn-inverse btn-rounded btn-bordred waves-effect waves-light w-md m-b-15 m-r-10">
                            Boleta
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
