<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //admin
        User::create([
            'name' => 'Administrador',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123123123'),
            'role' => User::ADMIN
        ]);

        //user

        User::create([
            'name' => 'Uusuario 01',
            'email' => 'user_01@gmail.com',
            'password' => bcrypt('123123123'),
            'address' => 'av La Paz 147 - Chosica',
            'phone' => '999999999',
            'dni' => '99999999',
            'birthday' => '1990-01-01',
            'code' => '9021'
        ]);
    }
}
